package com.jessecorbett.diskord.example

import com.jessecorbett.diskord.bot.bot
import com.jessecorbett.diskord.bot.events


suspend fun main() {
    bot("your-bot-token") {
        events {
            onMessageCreate { message ->
                if (message.content == "ping") {
                    message.reply("pong")
                }
            }
        }
    }
}
