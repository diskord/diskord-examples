package com.jessecorbett.diskord.example

import com.jessecorbett.diskord.bot.bot
import com.jessecorbett.diskord.bot.interaction.interactions
import kotlinx.coroutines.delay

suspend fun main() {
    bot("your-bot-token") {
        interactions {
            slashCommand("imagine", "Imagine something.") {
                val describe by stringParameter("describe", "What are you imagining?")

                callback {
                    acknowledgeForFutureResponse(ephemeral = true)

                    delay(1_000)

                    respond {
                        content = "You imagined: $describe"
                    }
                }
            }
        }
    }
}
