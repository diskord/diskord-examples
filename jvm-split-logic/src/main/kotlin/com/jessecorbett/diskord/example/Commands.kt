package com.jessecorbett.diskord.example

import com.jessecorbett.diskord.bot.CommandBuilder

/**
 * Respond to the `".help"` command with some useful information.
 */
fun CommandBuilder.bindHelp() {
    command("help") { message ->
        message.reply("I need somebody!")
    }
}
