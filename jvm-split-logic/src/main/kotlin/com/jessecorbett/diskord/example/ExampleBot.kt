package com.jessecorbett.diskord.example

import com.jessecorbett.diskord.bot.*


suspend fun main() {
    bot("your-bot-token") {
        bindHelp()
        bindPingResponse()
    }
}

/**
 * Bind an event handler to [Bot.messageCreated] which responds to `"ping"` with the message `"pong"`.
 */
fun BotBase.bindPingResponse() {
    events {
        onMessageCreate { message ->
            if (message.content == "ping") {
                message.reply("pong")
            }
        }
    }
}

/**
 * Bind a help command which itself is bound by another function.
 */
fun BotBase.bindHelp() {
    classicCommands {
        bindHelp()
    }
}
