package com.jessecorbett.diskord.example

import com.jessecorbett.diskord.api.channel.ChannelClient
import com.jessecorbett.diskord.bot.bot
import com.jessecorbett.diskord.bot.classicCommands
import com.jessecorbett.diskord.internal.client.RestClient
import com.jessecorbett.diskord.util.sendMessage
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Executors

private const val BOT_TOKEN = "your bot token"
private const val CHANNEL_ID = "your channel id"

private const val MESSAGE_COUNT = 60
private const val DELAY_MS = 60_000L

fun main() {
    val executor = Executors.newSingleThreadExecutor()
    val future = executor.submit {
        val client = RestClient.default(BOT_TOKEN)
        val channel = ChannelClient(CHANNEL_ID, client)
        for (count in 0..MESSAGE_COUNT) {
            runBlocking {
                channel.sendMessage("This is message ${count + 1} out of $MESSAGE_COUNT.")
            }

            try {
                Thread.sleep(DELAY_MS)
            } catch (ex: InterruptedException) {
                break
            }
        }
    }

    runBlocking {
        bot(BOT_TOKEN) {
            classicCommands {
                command("shutdown") { message ->
                    message.reply("Stopping bot.")

                    shutdown()

                    // cancel the send message action
                    future.cancel(true)
                    executor.shutdownNow()
                }
            }
        }
    }
}
